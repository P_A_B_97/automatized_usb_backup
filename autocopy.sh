#!/usr/bin/bash 

BACKUP_SOURCE="/path/to/filesforbackup"
BACKUP_DEVICE="/dev/xxxx"
MOUNT_POINT="/path/to/mountpoint"


# Verify if the mount point was created, if not create the mount point
if [ ! -d "MOUNT_POINT" ] ; then
  /bin/mkdir "$MOUNT_POINT";
fi

# Mount device in the selected path
/bin/mount "$BACKUP_DEVICE" "$MOUNT_POINT"

# Execute  backup:
/usr/bin/rsync -auz "$BACKUP_SOURCE" "$MOUNT_POINT" 

# Or make the backup and umount usb:
#/usr/bin/rsync -auz "$BACKUP_SOURCE" "$MOUNT_POINT" && /bin/umount "$BACKUP_DEVICE"
#exit

