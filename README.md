# AUTOMATIC USB BACKUP 

Automatic backup using udev rules for start a Systemd service and a shell script.


## FILES

There are 3 files (a .sh script, an udev rule and the systemd service) plus a README.md


#### The Shell Script

This shell script mount the device in the path described by the user. Then use the path of the source for the backup.


autocopy.sh



> #!/usr/bin/bash 
>
>BACKUP_SOURCE="/path/to/filesforbackup"
>BACKUP_DEVICE="/dev/usb_link"
>MOUNT_POINT="/path/to/mountpoint"
>
> #### Verify if the mount point is created, if not; create the directory
>if [ ! -d "MOUNT_POINT" ] ; then
>  /bin/mkdir "$MOUNT_POINT";
>fi
>
> #### Mount the usb device in the selected mount point
>/bin/mount "$BACKUP_DEVICE" "$MOUNT_POINT"
>
> #### Execute differential backup
>/usr/bin/rsync -auz "$BACKUP_SOURCE" "$MOUNT_POINT" 
>
> #### Uncomment this lines if you want umount automatically the drive after the backup 
> #/usr/bin/rsync -auz "$BACKUP_SOURCE" "$MOUNT_POINT" && /bin/umount "$BACKUP_DEVICE"
>
> #exit




#### Udev rule

This rule detect the usb and start a defined Systemd service. mv or cp in /etc/udev/rules.d (you need sudo permissions):

10.autocopy.rules



> SUBSYSTEM=="block", ACTION=="add", ATTRS{idProduct}=="xxxx", SYMLINK+="usb_link", ENV{SYSTEMD_WANTS}="usb_backup.service"



#### Systemd service


Systemd service that initiates the shell script.

usb_backup.service



>[Unit]
>Description=Automatic backup in a USB
>
>[Service]
>ExecStart=/path/to/autocopy.sh



## Instructions

Insert the USB open the terminal and type "lsusb". This command will display info. about your usb (ids). The ids are 8 numbers separated by a colon. The first 4 digits are the idVendor, the last 4 are the idProduct. Copy the idProduct and insert the 4 digits in the udev rule parameter "ATTRS{idProduct}==". Edit the "SYMLINK" parameter and write the name for the usb device. 
When this rule detect the usb (because de ATTRS{idProduct} parameter) it will create a SYMLINK in /dev with the name you wrote in SYMLINK.
This rule will detect the usb and activate the systemd service usb_backup.service
Once you finish editing the udev rule, copy or move it in to /etc/udev/rules.d (sudo permissions required)

Next thing is modify the systemd service. In the parameter ExecStart write your selected path for the .sh script.
Like the udev rule, you must move it in the /etc/systemd/system path.
The function of the service is very simple; start the shell script.

Edit the shell script. You must modify the 3 variables; first the directory of the files to backup. Second the name of the symlink in the udev rule for the usb device (SYMLINK+="xxxx") and the last one is the path of the mount point.

I left 2 options for backup, one of them commented. The default option is a simple differential backup; plug the usb and the script start automatically mounting the device and making the backup. You must umount manually.
The second option is the same operation but when the backup is done the script will umount automatically the device; you dont have to do anything.

If you select this last option make sure the backup is properly done and the usb is unmounted (with sudo journalctl -fu usb_backup)

Once you edit the files with your custom paths and symlink, the only thing you have to do is unplug and reconnect the USB. 



